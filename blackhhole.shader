shader_type canvas_item;

uniform float bh_radius = 200.0;
uniform vec2 bh_position = vec2 (600.0, 100.0);
uniform float blur_radius = 12.0;
uniform float blur_step = 3.0;


float circle_factor(vec2 origin, float r, vec2 current) {
	float len = distance(origin, current);
	
	return len/r;//clamp(1.0, 0.0, len/r);
}



void fragment(){
	//float circle_fac = circle_factor(vec2(screen_size.x/2.0, screen_size.y/2.0), 100.0, FRAGCOORD.xy);
	float circle_fac = circle_factor(bh_position, bh_radius , FRAGCOORD.xy);
	float blur_fac = 1.0 - abs(circle_fac - 1.0) / 1.2;
	vec4 color = vec4(0.0);
	// do blur on the edge
	if (circle_fac < 1.2 ){
		for( float i = 0.0 ;i < blur_radius * blur_step; i = i + blur_step){
			mat2 rot = mat2(vec2(cos(30.0), -1.0),vec2(4.0, 2.0));
			float time_distort_x = 0.3 + abs(sin(TIME * 1.93));
			float time_distort_y = 1.0 + abs(cos(TIME * 2.1));
			color = color + texture(SCREEN_TEXTURE, (SCREEN_UV + vec2(i,0.0) * time_distort_x  * rot * SCREEN_PIXEL_SIZE));
			color = color + texture(SCREEN_TEXTURE, (SCREEN_UV + vec2(-i,0.0)* time_distort_y * rot * rot * SCREEN_PIXEL_SIZE));
			color = color + texture(SCREEN_TEXTURE, (SCREEN_UV + vec2(0.0,i)* time_distort_x * rot * SCREEN_PIXEL_SIZE));
			color = color + texture(SCREEN_TEXTURE, (SCREEN_UV + vec2(0.0,-i)* time_distort_y *  SCREEN_PIXEL_SIZE));
		}
		color = color / (blur_radius / blur_step * 4.0);
		// --ahead the glowing ring
		/*if(circle_fac >= 0.8){
		color.rgb += vec3(1.0 + 0.5*sin(TIME * 0.3), 1.0 + 0.7 * cos(TIME * 0.5) , 1.0) * pow(1.0 - abs(circle_fac - 1.0)/0.2, 30.0);
		}*/
		float distort = clamp( (circle_fac - 1.2) / 0.2 , 0.0, 1.0);
		vec4 outer_distortion = texture(SCREEN_TEXTURE, (SCREEN_UV + normalize((FRAGCOORD.xy - bh_position)) * 0.2 * pow(distort, 2.0)));
		color = mix(color, outer_distortion, clamp( distort, 0.0, 1.0));
	}
	// do 'pull' inside of the circle
	vec4 color_distort = vec4(0.0);
	if (circle_fac < 1.0) {
		float distort_intensity = (1.0 - circle_fac) / (1.3 + 0.4 * abs(sin(TIME * 113.0 + 2.3) + 0.1));
		color_distort = texture(SCREEN_TEXTURE, (SCREEN_UV + normalize((FRAGCOORD.xy - bh_position)) * distort_intensity * (bh_radius * 0.8) * SCREEN_PIXEL_SIZE));
	}
	COLOR = color_distort;
	if(circle_fac <= 1.0){
		//mix between blur and pull
		COLOR = vec4(vec3((circle_fac - 0.8) / 0.2), 1.0);
		COLOR = mix(color_distort, color, pow(clamp((circle_fac - 0.8) / 0.2, 0.0, 1.0), 2.0));
	}
	else {
		//mix between blur and regular color
		COLOR = mix(color, texture(SCREEN_TEXTURE, SCREEN_UV), clamp((circle_fac - 1.0) / 0.2, 0.0, 1.0))
	}
	
}
