extends Node2D

var label = Label.new()
var viewport = Viewport.new()
var sprite = Sprite.new()
var camera = Camera.new()
var size = Vector2(0, 0)
export var padding = 2
export var corner = 3
export var text = "CIAOOO" setget set_text
export var backgroundColor = Color(0, 0, 0)
export(DynamicFont) var font 

func set_text(val):
	text = val
	update_text()

func update_text():
	label.add_font_override("", font)
	size = label.get_size()
	viewport.size = size
	var rtt = viewport.get_texture()
	sprite.texture = rtt
	update()

func set_viewport():
	viewport.transparent_bg = true
	viewport.render_target_v_flip = true
	viewport.add_child(label)
	viewport.add_child(camera)
	viewport.set_clear_mode(Viewport.CLEAR_MODE_ONLY_NEXT_FRAME)

func set_scene():
	add_child(sprite)
	add_child(viewport)

func _draw():
	var s = Vector2(size.x + padding, size.y + padding)
	var o = Vector2(s.x, s.y)/-2
	var c = corner
	var b = backgroundColor
	draw_rect(Rect2(o,s),b)
	draw_rect(Rect2(o+Vector2(0,-c),  Vector2(s.x,c)),b)
	draw_circle(o,c,b)
	draw_rect(Rect2(o+Vector2(s.x,0),Vector2(c,s.y)),b)
	draw_circle(o+Vector2(s.x,0),c,b)
	draw_rect(Rect2(o+Vector2(0,s.y),Vector2(s.x,c)),b)
	draw_circle(o+Vector2(0,s.y),c,b)
	draw_rect(Rect2(o+Vector2(-c,0),Vector2(c,s.y)),b)
	draw_circle(o+Vector2(s.x,s.y),c,b)

func _ready():
	label.text = text
	set_scene()
	set_viewport()
	update_text()
	update()