# Boing Hole

A small game made for [Godot Wild Jam #8](https://itch.io/jam/godot-wild-jam-8) on the theme *black hole*

## License

MIT

## Thanks to:

- [obsydianx](https://obsydianx.itch.io/interface-sfx-pack-1) for the sounds


## Todo:

- [ ] simplify the overly complex hierarchical structure (low importance)
- [ ] fix lateral collisions
- [ ] test inertia for the platform
- [ ] test having 4 platforms rotating in directions opposed to each other (so whatever direction you press, 1 platform will be moving - correctly)
- [x] shader for the black hole
- [x] fix elements colliding together (it completely stops the momentum)
- [x] fix collisions direction
- [x] fix text renderer thingy
- [x] game states (play now, game over)
  - [x] make game over screen pretty
- [x] make more pretty
- [x] add mobile controls
- [x] screen shake
- [x] particles should appear at contact point

## Building Android on Arch

Needed packages: 
```sh
yay -S android-sdk android-ndk jre8-openjdk jdk8-openjdk openjdk8-doc
```

run:
```sh
keytool -genkey -v -keystore release.keystore -alias boinghole -keyalg RSA -keysize 2048 -validity 10000
keytool -genkey -v -keystore debug.keystore -storepass android -alias boingholedebug -keypass android -keyalg RSA -keysize 2048 -validity 10000
```

Open Editor Settings and set:
- Export > Android > Adb: `/usr/bin/adb`
- Export > Android > jarsigner: `/usr/lib/jvm/java-8-openjdk/bin/jarsigner`
- Export > Android > debug keystore: `debug.keystore` (the file you generated above)