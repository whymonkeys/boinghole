# Modification of original scripy by
# https://twitter.com/_Azza292
# https://pastebin.com/pjxjqyrK
# https://pastebin.com/pSV9vVP1
# Adapted for Camera2D
# MIT License
extends Camera2D
 
export var speed := 1.0
export var decay_rate := 0.5
# warning-ignore:unused_class_variable
export var max_offset := 20
# warning-ignore:unused_class_variable
export var max_rotation := 1
# default values
onready var start_position := offset
onready var start_rotation := rotation
var trauma := 0.0
var time := 0.0
var noise := OpenSimplexNoise.new()
var noise_seed := randi()
 
func _init() -> void:
	set_rotating(true)
	noise.seed = noise_seed
	noise.octaves = 1
	noise.period = 256.0
	noise.persistence = 0.5
	noise.lacunarity = 1.0
 
func _process(delta: float) -> void:
	if trauma > 0.0:
		decay_trauma(delta)
		apply_shake(delta)
	
func add_trauma(amount: float) -> void:
	trauma = min(trauma + amount, 1.0)
	
func decay_trauma(delta: float) -> void:
	var change := decay_rate * delta
	trauma = max(trauma - change, 0.0)

func get_noise_value(seed_value: int, time: float) -> float:
	noise.seed = seed_value
	return noise.get_noise_2d(time, time)

func get_noisy_float(time:float, max_val: float, shake: float, seed_offset:int ) -> float: 
	var fl := max_val * shake * get_noise_value(noise_seed + seed_offset, time)
	return fl
	
func get_noisy_float_array(delta:float, maxes:PoolRealArray) -> PoolRealArray:
	time += delta * speed * 5000.0
	var shake := trauma * trauma
	var result:PoolRealArray = []
	for i in range(maxes.size()):
		var max_val = maxes[i]
		var val:float = get_noisy_float(time,max_val,shake,i)
		result.append(val)
	return result
	
func apply_shake(delta: float) -> void:
		var values:PoolRealArray = get_noisy_float_array(delta, [max_offset, max_offset, max_rotation])
		var offset_x := values[0]
		var offset_y := values[1]
		var roll := values[2]
		offset = start_position + Vector2(offset_x, offset_y)
		rotation = start_rotation + roll