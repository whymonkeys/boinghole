extends AudioStreamPlayer

export(Array, String, FILE) var tracks = []
export var directory = ""

func list_files_in_directory(directory):
	var files = []
	var dir = Directory.new()
	dir.open(directory)
	dir.list_dir_begin()
	while true:
		var file = dir.get_next()
		if file == "":
			break
		elif not file.begins_with("."):
			files.append(file)
		
	dir.list_dir_end()
	return files

func _ready():
	if directory:
		tracks = list_files_in_directory(directory)
	randomize()

func play_random_song():
	var rand_nb = randi() % tracks.size()
	var track_path = tracks[rand_nb]
	var audiostream = load(track_path)
	set_stream(audiostream)
	play()