tool
extends Node2D

signal body_eaten(body)

export(float) var radius = 100 setget set_radius, get_radius
export var color = Color(0.0, 0.0, 0.0) setget set_color, get_color


func _ready():
	$Area2D.connect("body_entered", self, "_on_body_entered")


func print_vars():
	print(radius, color)


func set_radius(new_radius):
	if not has_node("arc"):
		return
	$Area2D/CollisionShape2D.shape.radius = float(new_radius)
	get_node("arc").radius = new_radius

func get_radius():
	if not has_node("arc"):
		return
	return get_node("arc").radius
		
func set_color(new_color):
	if not has_node("arc"):
		return
	get_node("arc").color = new_color

func get_color():
	if not has_node("arc"):
		return
	return get_node("arc").color


func _on_body_entered(body):
	$RandomPlayer.play_random_song()
	emit_signal("body_eaten", body)