extends Node2D

var mouse_button_pressed = false
var rot_pos = 0
var rotation_threshold = 0.01
var damping = 0.1 # 0.001 = very slow, 1 = no damping
signal wheeled(deg,rotation_delta, delta)

func _ready():
	set_process(true)
	$area.visible = false
	
func _process(delta: float) -> void:
		if(mouse_button_pressed):
			var mouse_pos = get_local_mouse_position()
			var angle = mouse_pos.angle()
			var new_rot_pos = rotation + angle
			var damped_rot_pos = rotation + angle * damping
			var deg_rot_pos = rad2deg(new_rot_pos)
			var rotation_delta = deg_rot_pos - rot_pos
			if abs(rotation_delta) >= rotation_threshold:
				rot_pos = deg_rot_pos
				emit_signal("wheeled", rot_pos,rotation_delta, delta)
				rotation = damped_rot_pos
		
func _input(event:InputEvent) -> void:
	if !is_valid_event(event): return
	var ok = point_is_within_bounds(event)
	if extract_pointer_idx(event) == -1: 
		return
	var is_pressed = pointer_is_pressed(event)
	# If state hasn't changed, don't do anything
	if (is_pressed == mouse_button_pressed):
		return
	elif is_pressed and ok:
		mouse_button_pressed = true
	elif pointer_is_released(event):
		mouse_button_pressed = false

func point_is_within_bounds(event):
	var size = $area.get_rect().size
	var rect = Rect2(position-size,size*2)
	var point_pos = event.position
	var is_within_bounds = rect.has_point(point_pos)
	# print(rect, " : ", point_pos, " = ", is_within_bounds)
	return is_within_bounds

func is_valid_event(event):
	if event is InputEventScreenTouch or \
		event is InputEventScreenDrag or \
		event is InputEventMouseButton or \
		event is InputEventMouseMotion:
			return true
	else:
		return false

func extract_pointer_idx(event):
	if event is InputEventScreenTouch or event is InputEventScreenDrag:
		return 1
	elif event is InputEventMouseButton or event is InputEventMouseMotion:
		return 0
	else:
		return -1

func pointer_is_pressed(event):
	if event is InputEventMouseMotion:
		return (event.button_mask == 1)
	elif event is InputEventScreenTouch:
		return event.is_pressed()

func pointer_is_released(event):
	if event is InputEventScreenTouch:
		return !event.is_pressed()
	elif event is InputEventMouseButton:
		return !event.is_pressed()
