extends Label

var score = 0.0

func _ready():
	randomize()
	add_to_group("paddle_listeners")

func add_score(val):
	score += val
	$AnimationPlayer.play("add_score")

func _process(delta):
	score += delta * PI
	text = "%.3f" % score

func on_paddle_bounce(body):
	add_score(body.mass * 10.0)