tool
extends Node2D

export var resolution = 32
export var thickness = 10
export var color = Color(1.0, 0.0, 0.0)
export var radius = 80
export var angle_start = 20
export var angle_end = 180
export var center = Vector2(0, 0)

func _ready():
		update()

func _process(delta):
		update()

func _draw():
		draw_arc()

func draw_arc():
		var res = resolution
		if(resolution == 0): 
			res = int(radius/6);
		
		var points_arc = PoolVector2Array()
		var colors = PoolColorArray([color])
		
		for i in range(res + 1):
				var angle_point = deg2rad(angle_start + i * (angle_end - angle_start) / res - 90)
				points_arc.push_back(center + Vector2(cos(angle_point), sin(angle_point)) * radius)

		if thickness:
			var radius2 = radius - thickness
			
			for n in range(res+1):
					var i = res - n
					var angle_point = deg2rad(angle_start + i * (angle_end - angle_start) / res - 90)
					points_arc.push_back(center + Vector2(cos(angle_point), sin(angle_point)) * radius2)
			
		draw_polygon(points_arc,colors)