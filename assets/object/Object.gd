extends RigidBody2D
export var text = "" setget set_text

func set_text(val):
	text = val
	$SpriteLabelGenerator.text = val

func _ready():
	yield(get_tree(), "idle_frame")
#	var sprite = $SpriteLabelGenerator.sprite
#	var tex = ImageTexture.new()
#	tex.create_from_image(sprite.texture.get_data())
#	var sprite2 = sprite.duplicate()
#	sprite2.texture = sprite.texture
#	add_child(sprite)
	var shape = RectangleShape2D.new()
	shape.extents = $SpriteLabelGenerator.viewport.size/2.0
	#Not sure why i have to divide by two?
	$CollisionShape2D.shape = shape
#	$SpriteLabelGenerator.queue_free()
	add_to_group("paddle_listeners")

func on_paddle_bounce(body, point):
	if(body == self):
		#$CPUParticles2D.global_position = point
		#$CPUParticles2D.emitting = true
		pass
		
func set_velocity(vel):
	linear_velocity = vel

func set_angular_velocity(ang):
	angular_velocity = ang

func _on_VisibilityNotifier2D_screen_exited():
	queue_free()
