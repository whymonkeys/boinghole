tool
extends Node2D

var particles = preload("res://assets/particles.tscn")
export var radius = 100 setget set_radius, get_radius
export var color = Color(0.0, 0.0, 0.0) setget set_color, get_color
export var angle_start = 0 setget set_angle_start, get_angle_start
export var angle_end = 20 setget set_angle_end, get_angle_end
export var thickness = 20 setget set_thickness, get_thickness
export var paddle_bounce_factor = 1.0

func _ready():
	$Area2D.connect("body_entered", self, "_on_body_entered")

func get_vars():
	print(radius,color, angle_start,angle_end,thickness)

func set_radius(new_radius):
	if not has_node("arc"):
		return
	get_node("arc").radius = new_radius
	get_node("Area2D/CollisionShape2D").shape.radius = new_radius

func get_radius():
	return get_node("arc").radius

func set_color(new_color):
	if not has_node("arc"):
		return
	get_node("arc").color = new_color

func get_color():
	return get_node("arc").color

func set_angle_start(new_var):
	if not has_node("arc"):
		return
	get_node("arc").angle_start = new_var

func get_angle_start():
		return get_node("arc").angle_start
		
func set_angle_end(new_var):
	if not has_node("arc"):
		return
	get_node("arc").angle_end = new_var

func get_angle_end():
		return get_node("arc").angle_end
		
func set_thickness(new_thickness):
	if not has_node("arc"):
		return
	get_node("arc").thickness = new_thickness

func get_thickness():
	return get_node("arc").thickness

func sparkle():
	var spark = particles.instance()
	add_child(spark)
	$RandomPlayer.play_random_song()
	var outer_size = get_radius()
	var mid_angle = deg2rad(get_angle_start()+20)
	spark.position = Vector2(0,-1*outer_size).rotated(mid_angle)
	spark.rotation = mid_angle

func _on_body_entered(body: RigidBody2D):
	var vec_to_body = (body.global_position - global_position)
	var angle = Vector2.UP.angle_to(vec_to_body.normalized())
	var paddle_width = abs(self.angle_end - self.angle_start)
	var paddle_center_vector = Vector2.UP.rotated(deg2rad((self.angle_start + paddle_width/2.0)))
	
	if abs(vec_to_body.normalized().angle_to(paddle_center_vector)) < deg2rad(paddle_width/2.0) and\
			vec_to_body.length() > self.radius - self.thickness:
		sparkle()
		body.linear_velocity = -body.linear_velocity * paddle_bounce_factor
		body.angular_velocity = -body.angular_velocity * paddle_bounce_factor
		get_tree().call_group("paddle_listeners", "on_paddle_bounce" , body, body.global_position)