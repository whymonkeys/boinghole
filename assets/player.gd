tool
extends Node2D

signal bh_oversized()

var bh_max_size = 0.0
export var paddle_position = 0 setget set_position
export var paddle_size = 40 setget set_paddle_size
export var radius = 100 setget set_radius,get_radius
export var distance = 10 setget set_distance
signal body_eaten(body)

func _ready():
	position = get_viewport().size/2.0
	var view = get_viewport()
	var min_size: float = view.size.x if view.size.x < view.size.y else view.size.y
	bh_max_size = min_size / 2.0 * 0.7
	print("Max size for BH is: " + str(bh_max_size))
	get_viewport().connect("size_changed", self, "_on_viewport_resize")
	get_node("black-hole").connect("body_eaten", self, "_on_blackhole_body_eaten")

func set_distance(new_dist):
	distance = new_dist
	set_paddle()

func set_paddle():
	if not has_node("paddle"):
		return
	get_node("paddle").radius = radius + distance + get_node("paddle").thickness
	get_node("paddle").angle_start = paddle_position - paddle_size/2.0
	get_node("paddle").angle_end = paddle_position + paddle_size/2.0

func set_black_hole():
	if not has_node("black-hole"):
		return
	get_node("black-hole").radius = radius

func set_position(new_pos):
	paddle_position = new_pos
	set_paddle()
	
func set_paddle_size(new_size):
	paddle_size = new_size
	set_paddle()
	
func set_radius(new_radius):
	radius = new_radius
	set_paddle()
	set_black_hole()

func get_radius():
	return get_node("black-hole").radius


func _on_blackhole_body_eaten(body):
	# TODO grow!!
	set_radius(self.get_radius() + 5.0 * body.mass)
	yield(get_tree().create_timer(0.5), "timeout")
	body.queue_free()
	if self.get_radius() >= bh_max_size:
		emit_signal("bh_oversized")


func _on_viewport_resize():
	position = get_viewport().size / 2.0