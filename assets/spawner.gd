extends Node
export var words_list = ["batata", "banana", "ananas"]
export var currentIndex = 0
export var time_between_spawns = 3.0
export var circle_spawner_size = 600
export var initial_speed_factor = 100.0
var speed_factor = 100.0
export var speed_growth_factor = 1
export(PackedScene) var object_to_spawn
var timer

func load_file():
	var f = File.new()
	var path = self.get_script().get_path().get_base_dir() + "/words.txt"
	f.open(path, File.READ)
	var content = f.get_as_text()
	var words = Array(content.split("\n"))
	f.close()
	words.shuffle()
	return words
	
func _ready():
	speed_factor = initial_speed_factor
	timer = Timer.new()
	timer.wait_time = time_between_spawns
	add_child(timer)
	timer.start()
	timer.connect("timeout", self, "spawn_object")
	words_list = load_file()
	randomize()

func get_next_word():
	var word = words_list[currentIndex]
	if(currentIndex < words_list.size() - 1):
		currentIndex = currentIndex + 1
	else:
		currentIndex = 0
	return word

func spawn_object():
	var direction = Vector2((randf() - 0.5) * 2.0, (randf() - 0.5) * 2.0).normalized()
	var pos = get_viewport().size / 2.0 + direction * circle_spawner_size
	var obj = object_to_spawn.instance()
	obj.text = get_next_word()
	obj.set_velocity(- direction * speed_factor)
	obj.set_angular_velocity(randf() * PI)
	add_child(obj)
	obj.position = pos

func reset():
	speed_factor = initial_speed_factor

func _process(delta):
	speed_factor += speed_growth_factor*delta