extends ColorRect

export (NodePath) var player_path
onready var _player = get_node(player_path)


func _process(delta):
	# TODO OPTIMIZEEEHHH
	material.set_shader_param("bh_radius", _player.radius)
	material.set_shader_param("bh_position", _player.global_transform.origin)