extends CanvasLayer

signal play_again

func _ready():
	var anim = $AnimationPlayer
	anim.play("hide")
	anim.seek(anim.current_animation_length)
	$Button.connect("pressed", self, "hide")

func show():
	$Sprite.global_transform.origin = get_viewport().size/2.0
	$AnimationPlayer.play("show")

func hide():
	$AnimationPlayer.play("hide")
	yield($AnimationPlayer, "animation_finished")
	emit_signal("play_again")
