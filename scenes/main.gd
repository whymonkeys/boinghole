extends Node2D

var initial_state = {}

func _ready():
	print("ready")
	$player.connect("bh_oversized", self, "end_game")
	$EndScreen.connect("play_again", self, "_on_play_again")
	add_to_group("paddle_listeners")
	var player = $player
	initial_state = {
	"radius": player.radius,
	"paddle_position": player.paddle_position,
	"distance": player.distance,
	"paddle_size": player.paddle_size
	}

func on_paddle_bounce(body, pos):
	$player/Camera2D.add_trauma(.3)
	
func _process(delta):
	var player = get_node("player")
	if Input.is_action_pressed("ui_right"):
		player.paddle_position+=300.0*delta
	
	if Input.is_action_pressed("ui_left"):
		player.paddle_position-=300.0*delta

func end_game():
	get_tree().paused = true
	$CanvasLayer/Distortion.visible = false
	$EndScreen.show()

func _on_play_again():
	$CanvasLayer/Distortion.visible = true
	get_tree().paused = false
	for key in initial_state.keys():
		$player.set(key, initial_state[key])
	# Using two different methods ecause i though i was clever,
	# turns out I am not :D
	$Spawner.reset()

func _on_Wheel_wheeled(deg, rotation_delta, delta):
	$player.paddle_position+=rotation_delta

func _on_SoundButton_toggled(button_pressed):
	if not button_pressed:
		AudioServer.set_bus_mute(AudioServer.get_bus_index("Master"),true)
		AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Master"), 0)
	else:
		AudioServer.set_bus_mute(AudioServer.get_bus_index("Master"),false)
		AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Master"), 1)

func _on_SoundButton_pressed():
	print("ok!")
	$SoundButton.pressed = !$SoundButton.pressed
